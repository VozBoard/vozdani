//
//  ViewController.m
//  KeyBoardAdapt
//
//  Created by Bruno Roberto on 13/12/14.
//  Copyright (c) 2014 bruno. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController{
    //Variaveis utilizadas para comunicacao WIFI
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    BOOL flagWIFI;
    NSString *point;
    BOOL flagCapsLocker;
    
    UIButton *button;
    int flagSize;
    int flagKeyBoard;
    NSArray *keyBoard1;
    NSArray *keyBoard2;
    NSArray *keyBoard3;
    NSString *idioma;
    
    AVSpeechSynthesizer *speechSyn;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    self.scroll.contentSize = CGSizeMake(1000, 0);
    flagKeyBoard=1;
    flagCapsLocker= false;
    speechSyn = [AVSpeechSynthesizer new];
    
    speechSyn.delegate = self;
    self.outletText.delegate = self;
    
    //Desaparecer o teclado
    UIView* dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    self.outletText.inputView = dummyView; // Hide keyboard, but show blinking cursor
    
    keyBoard1 = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"0",@"á",@"ã",@"é",@"ê",@"í",@"ó",@"õ",@"ú",@",",@".",@"?",@"!",@"-",@";",@":",@"+",@"=", nil];
    
    keyBoard2 = [[NSArray alloc] initWithObjects:@"q",@"w",@"e",@"r",@"t",@"y",@"u",@"i",@"o",@"p",@"a",@"s",@"d",@"f",@"g",@"h",@"j",@"k",@"l",@"ç",@"z",@"x",@"c",@"v",@"b",@"n",@"m", nil];
    
    keyBoard3 = [[NSArray alloc] initWithObjects:@"Q",@"W",@"E",@"R",@"T",@"Y",@"U",@"I",@"O",@"P",@"A",@"S",@"D",@"F",@"G",@"H",@"J",@"K",@"L",@"Ç",@"Z",@"X",@"C",@"V",@"B",@"N",@"N", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - action
- (IBAction)keyConfig:(UIButton *)sender {
    [self.outletText selectAll:self];
}

- (IBAction)actionsKey:(UIButton *)sender {
    [self.outletText insertText:[sender currentTitle]];
    if (flagCapsLocker) {
        flagCapsLocker = false;
        for (int i=0; i < keyBoard2.count; i++) {
            [self.listKeyBoard[i] setTitle:keyBoard2[i] forState:UIControlStateNormal];
        }
    }
//    [self speak:[sender currentTitle]];
}

- (IBAction)switchKey:(UIButton *)sender {
    if(flagKeyBoard==1){
        flagKeyBoard = 2;
        for (int i=0; i < keyBoard1.count; i++) {
            [self.listKeyBoard[i] setTitle:keyBoard1[i] forState:UIControlStateNormal];
        }
    }else if (flagKeyBoard==2){
        flagKeyBoard=1;
        for (int i=0; i < keyBoard2.count; i++) {
            [self.listKeyBoard[i] setTitle:keyBoard2[i] forState:UIControlStateNormal];
        }
    }

}

- (IBAction)keySpace:(UIButton *)sender {
    [self.outletText insertText:@" "];
//    [speechSyn pauseSpeakingAtBoundary:AVsp]
}

- (IBAction)keyDelete:(UIButton *)sender {
    
    [self.outletText deleteBackward];
}

- (IBAction)keyPause:(UIButton *)sender {
    if ([speechSyn isPaused]) {
        [sender setTitle:@"Play" forState:UIControlStateNormal];
        [speechSyn continueSpeaking];
    }else{
        [sender setTitle:@"Pause" forState:UIControlStateNormal];
        [speechSyn pauseSpeakingAtBoundary:AVSpeechBoundaryImmediate];
    }
}

- (IBAction)playSpeak:(UIButton *)sender {
    [self speak:self.outletText.text];
}

- (IBAction)keyCursorEsquerda:(UIButton *)sender {
    self.outletText.selectedRange = NSMakeRange(self.outletText.selectedRange.location-1, 0);
}

- (IBAction)keyCursorDireita:(UIButton *)sender {
    self.outletText.selectedRange = NSMakeRange(self.outletText.selectedRange.location+1, 0);
}

- (IBAction)keyCapsLoker:(UIButton *)sender {
    if (!flagCapsLocker) {
        flagCapsLocker = true;
        for (int i=0; i < keyBoard2.count; i++) {
            [self.listKeyBoard[i] setTitle:keyBoard3[i] forState:UIControlStateNormal];
        }
    }else{
        
        flagCapsLocker = false;
        for (int i=0; i < keyBoard2.count; i++) {
            [self.listKeyBoard[i] setTitle:keyBoard2[i] forState:UIControlStateNormal];
        }
    }
    
}

- (IBAction)btnVoltar:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - function Speech
-(void) speak:(NSString *)res{
    idioma =  NSLocalizedString(@"idioma", nil);
    if (!speechSyn.isSpeaking) {
        AVSpeechUtterance *speechUtterace = [[AVSpeechUtterance alloc] initWithString:res];
        //Speed
        [speechUtterace setRate:AVSpeechUtteranceDefaultSpeechRate];
        //You want diferent vocie
//        NSLog(@"%@" ,[AVSpeechSynthesisVoice speechVoices]);
        speechUtterace.voice = [AVSpeechSynthesisVoice voiceWithLanguage:idioma];
        [speechSyn speakUtterance:speechUtterace];
    }
}


#pragma mark - AVSpeechSynthesizerDelegate

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer willSpeakRangeOfSpeechString:(NSRange)characterRange utterance:(AVSpeechUtterance *)utterance {
//    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    if (![self.outletText.text isEqualToString:@"point"]) {
        NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:self.outletText.text];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:characterRange];
        self.outletText.attributedText = mutableAttributedString;
        [self.outletText setFont:[UIFont boldSystemFontOfSize:60]];
    }
}
//Start
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    self.outletText.attributedText = [[NSAttributedString alloc] initWithString:self.outletText.text];
}
//Pause
-(void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didPauseSpeechUtterance:(AVSpeechUtterance *)utterance{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
}

-(void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didContinueSpeechUtterance:(AVSpeechUtterance *)utterance{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
}

//Finish
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance{
    NSLog(@"%@ %@", [self class], NSStringFromSelector(_cmd));
    self.outletText.attributedText = [[NSAttributedString alloc] initWithString:self.outletText.text];
    [self.outletText setFont:[UIFont boldSystemFontOfSize:60]];
}

@end
