//
//  Action.h
//  BoardCommunication
//
//  Created by Bruno Roberto on 18/12/14.
//  Copyright (c) 2014 bruno. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Action : NSObject

@property (nonatomic,assign) NSString *name;
@property (nonatomic,assign) NSString *img;
@property (nonatomic,assign) NSString *sound;
@property (nonatomic,assign) NSString *music;
@property (nonatomic) UIColor *background;

@property (nonatomic) NSMutableArray *listChildren;
@property (nonatomic) int indice;
- (id)init;
-(void)startLlist;
-(BOOL) isParent;
-(void)addChildren:(Action *) action;
@end
