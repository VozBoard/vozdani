//
//  AppDelegate.h
//  BoardCommunication
//
//  Created by Bruno Roberto on 18/12/14.
//  Copyright (c) 2014 bruno. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

