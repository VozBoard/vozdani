//
//  Screen.h
//  BoardCommunication
//
//  Created by Bruno Roberto on 26/01/15.
//  Copyright (c) 2015 bruno. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Screen : NSObject
@property (nonatomic) NSMutableArray *listActions;
- (id)init;
@end
