//
//  Action.m
//  BoardCommunication
//
//  Created by Bruno Roberto on 18/12/14.
//  Copyright (c) 2014 bruno. All rights reserved.
//
/*Essa classe sera responsavel por informacoes do Componente da prancha. Ou seja,
    ela ira informa a imagem do componente, o audio, o nome, se ele tem filhos, dentre outos.
*/
 #import "Action.h"

@implementation Action


-(void)startLlist{
self.listChildren =  [NSMutableArray new];
}

-(BOOL)isParent{
    if (self.listChildren.count!=0) {
        return true;
    }
    return false;
}
- (id)init{
    self = [super init];
    self.listChildren =  [NSMutableArray new];
//    if (self) {
//        return NULL;
//    }
    return self;
}

-(void)addChildren:(Action *)action{
    if (action.sound == nil ) {
        action.sound = action.name;
    }
    [self.listChildren addObject:action];
}
@end
