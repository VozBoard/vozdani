//
//  ViewController.h
//  BoardCommunication
//
//  Created by Bruno Roberto on 18/12/14.
//  Copyright (c) 2014 bruno. All rights reserved.
//

#import <UIKit/UIKit.h>

@import Foundation;
@import AVFoundation;


@interface MainViewController : UIViewController<AVSpeechSynthesizerDelegate>

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *listOutletButton;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *listOutletImage;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *listOutletNameActions;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
//

@property (strong,nonatomic) AVAudioPlayer *player;

//@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *listOutletImg;

- (IBAction)button1:(UIButton *)sender;
- (IBAction)button2:(UIButton *)sender;
- (IBAction)button3:(UIButton *)sender;
- (IBAction)button4:(UIButton *)sender;
- (IBAction)button5:(UIButton *)sender;
- (IBAction)buttonBack:(UIButton *)sender;































@end

