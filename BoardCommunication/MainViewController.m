//
//  ViewController.m
//  BoardCommunication
//
//  Created by Bruno Roberto on 18/12/14.
//  Copyright (c) 2014 bruno. All rights reserved.
//

/*Essa ViewController ira tratar as acoes que irao acontecer na
 criacao de Actions.
 */


#import "MainViewController.h"
#import "Action.h"
#import "Screen.h"

@interface MainViewController ()
//@property (nonatomic) NSMutableArray *listAction;
@end

@implementation MainViewController{
    NSMutableArray *listActionCurrent;
    NSMutableArray *listActions;
    Action *action1;
    Action *action2;
    Action *action3;
    Action *action4;
    Action *action5;
    Action *actionEmpity;
    Action *buttonCliked;
    NSNumber *buttonClikedIndex;
    int numberScreen;
    AVSpeechSynthesizer *speechSyn;
    BOOL flagActivetActionButton;
    NSString *idioma;
    NSString *textoFalado;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    speechSyn = [AVSpeechSynthesizer new];
    speechSyn.delegate = self;
    
    actionEmpity = [[Action alloc] init];
    [self completActions];
    [self completActions2];
    [self completActions3];
    [self completActions4];
    [self completActions5];
    
    listActionCurrent = [[NSMutableArray alloc] init];
    listActions = [NSMutableArray new];
    
    [self addListActions:action1];
    [self addListActions:action2];
    [self addListActions:action3];
    [self addListActions:action4];
    [self addListActions:action5];
    
    [self addListActionCurrent:action1];
    [self addListActionCurrent:action2];
    [self addListActionCurrent:action3];
    [self addListActionCurrent:action4];
    [self addListActionCurrent:action5];
    
    
    flagActivetActionButton = true;
    numberScreen=0;
    
    buttonCliked = [[Action alloc] init];
    
    Action *res = [[Action alloc] init];
//    [self.listOutletImage[1] setImage:[UIImage imageNamed:res.img]];
    
    for (int i=0; i <  listActionCurrent.count; i++) {
        res = listActionCurrent[i];
        [self.listOutletNameActions[i] setText:res.name];
        [self.listOutletButton[i] setHidden:NO];
//        [self.listOutletButton[i] setBackgroundImage:[UIImage imageNamed:res.img] forState:UIControlStateNormal];
//        [self.listOutletImage[i] setImg:res.img];
        [self.listOutletImage[i] setImage:[UIImage imageNamed:res.img]];
        [self.listOutletButton[i] setBackgroundColor:res.background];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - function action

- (IBAction)button1:(UIButton *)sender {
//    if (flagActivetActionButton) {
        Action *res = listActionCurrent[0];
    if (![res isParent]) {
        buttonClikedIndex = 0;
    }
        //buttonCliked = res;
        [self speak:res.sound];
        if ([res isParent] && flagActivetActionButton){
            flagActivetActionButton = false;
            [self logicAction:res];
        }
//    }
}

- (IBAction)button2:(UIButton *)sender {
    Action *res = listActionCurrent[1];
    buttonCliked = res;
    [self speak:res.sound];
    if ([res isParent] && flagActivetActionButton){
        flagActivetActionButton = false;
        [self logicAction:res];
    }
}

- (IBAction)button3:(UIButton *)sender {
    
    Action *res = listActionCurrent[2];
    buttonCliked = res;
    [self speak:res.sound];
    if ([res isParent] && flagActivetActionButton){
        flagActivetActionButton = false;
        [self logicAction:res];
    }
}

- (IBAction)button4:(UIButton *)sender {
//    [self playSound:@"bipe"];
    Action *res = listActionCurrent[3];
    buttonCliked = res;
    if (res.music!=NULL) {
        [self playSound:res.music];
    }else{
        [self speak:res.sound];
    }

    if ([res isParent] && flagActivetActionButton){
        flagActivetActionButton = false;
        [self logicAction:res];
    }
}

- (IBAction)button5:(UIButton *)sender {
    Action *res = listActionCurrent[4];
    buttonCliked = res;
    if (res.music!=NULL) {
        [self playSound:res.music];
    }else{
        [self speak:res.sound];
    }
    
    if ([res isParent] && flagActivetActionButton){
        flagActivetActionButton = false;
        [self logicAction:res];
    }
}

- (IBAction)buttonBack:(UIButton *)sender {
    self.buttonBack.backgroundColor = [UIColor colorWithRed:(255/255.0) green:206/255.0 blue:84/255.0 alpha:1];
    if (numberScreen >= 1) {
        numberScreen-=5;
    Action *res = [[Action alloc] init];
    for (int i=0; i < 5; i++) {
        res = listActions[i+numberScreen];
        [listActions removeLastObject];
        [listActionCurrent setObject:res atIndexedSubscript:i];
        [self.listOutletNameActions[i] setText:res.name];
//        [self.listOutletButton[i] setBackgroundImage:[UIImage imageNamed:res.img] forState:UIControlStateNormal];
        [self.listOutletImage[i] setImage:[UIImage imageNamed:res.img]];
        [self.listOutletButton[i] setHidden:NO];
        [self.listOutletButton[i] setBackgroundColor:res.background];
    }
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
       // self.buttonBack.backgroundColor = [UIColor colorWithRed:(255/255.0) green:206/255.0 blue:84/255.0 alpha:1];
    }
}

#pragma mark - functions
-(void)logicAction:(Action*)action{
    numberScreen+=5;
    Action *res = [[Action alloc] init];
    for (int i=0; i < 5; i++) {
        //Verifica se o numero de filhos e' igual a numero de Actions que devem ter na Cena.
        if (action.listChildren.count > i) {
            res = action.listChildren[i];
            [listActions addObject:res];
            [listActionCurrent setObject:res atIndexedSubscript:i];
            [self.listOutletNameActions[i] setText:res.name];
            //[self.listOutletNameActions[i] setColor:res.background];
            [self.listOutletButton[i] setHidden:NO];
//            [self.listOutletButton[i] setBackgroundImage:[UIImage imageNamed:res.img] forState:UIControlStateNormal];
            [self.listOutletImage[i] setImage:[UIImage imageNamed:res.img]];
            [self.listOutletButton[i] setBackgroundColor:res.background];
        }else {
            [listActions addObject:actionEmpity];
            [listActionCurrent setObject:actionEmpity atIndexedSubscript:i];
            [self.listOutletButton[i] setBackgroundImage:[UIImage imageNamed:@"AppIcon"] forState:UIControlStateNormal];
            [self.listOutletNameActions[i] setText:@""];
            [self.listOutletButton[i] setHidden:NO];
//            [self.listOutletImage[i] setImage:[UIImage imageNamed:res.img]];
            [self.listOutletButton[i] setBackgroundColor:res.background];
        }
    }
    [self.listOutletButton.lastObject setBackgroundColor:res.background];
}

-(void)addListActionCurrent:(Action *)action{
    if (action.sound == nil ) {
        action.sound = action.name;
    }
    [listActionCurrent addObject:action];
}

-(void)addListActions:(Action *)action{
    if (action.sound == nil ) {
        action.sound = action.name;
    }
    [listActions addObject:action];
}

- (void)playSound:(NSString *)nameMusic{
    // Load in a audio file from your bundle (all the files you have added to your app)
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:nameMusic ofType:@"mp3"];
    // Convert string to NSURL
    NSURL *audioURL = [NSURL fileURLWithPath:soundFilePath];
    // Initialize a audio player with the URL (the mp3)
    
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:audioURL error:nil];
    //self.player.volume = 1.0;
    [self.player play];
}

#pragma mark - function Speech
-(void) speak:(NSString *)res{
    idioma =  NSLocalizedString(@"idioma", nil);
    if (!speechSyn.isSpeaking) {
        AVSpeechUtterance *speechUtterace = [[AVSpeechUtterance alloc] initWithString:res];
        [speechUtterace setRate:AVSpeechUtteranceDefaultSpeechRate];
        speechUtterace.voice = [AVSpeechSynthesisVoice voiceWithLanguage:idioma]; // idioma = voiceWithLanguage:@"pt-BR"
        [speechSyn speakUtterance:speechUtterace];
        flagActivetActionButton = true;
        NSLog(@"%@", idioma);
        if ([res isEqualToString:@"Quando inicio uma conversa com uma pessoa desconhecida. Geralmente. Vem a famosa pergunta: “Mas… O que você tem?”"]) {
            textoFalado = res;
        }
//        if ([res isEqualToString:@"Certo dia, Eu estava no shopping, com meu amigo, e decidi comprar um livro..."]) {
//            textoFalado = res;
//        }
//        if ([res isEqualToString:@"Já existem soluções para uma pessoa com dificuldades para se expressar, como eu. Exemplo disso são os recursos utilizados aqui: este aplicativo, Voz, que foi desenvolvido por estudantes do Instituto Federal do Ceará. IFCE. A dança de cadeiras de rodas, entre outras coisas. É possível, para mim, sonhar com um futuro melhor. Mesmo com as sequelas da Paralisia Cerebral, sou feliz."]) {
//            textoFalado = res;
//        }
    }
}
//Start
-(void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance{
//    buttonCliked = listActionCurrent[buttonClikedIndex.integerValue];
//    buttonCliked.background = [UIColor colorWithRed:79/255.0 green:64/255.0 blue:119/255.0 alpha:1];
    
    
}
//Pause
-(void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didPauseSpeechUtterance:(AVSpeechUtterance *)utterance{
    //buttonCliked.background = [UIColor blueColor];
}
//Continue
-(void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didContinueSpeechUtterance:(AVSpeechUtterance *)utterance{
    //buttonCliked.background = [UIColor brownColor];
}
//Finish
-(void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance{
    if ([textoFalado isEqualToString:@"Quando inicio uma conversa com uma pessoa desconhecida. Geralmente. Vem a famosa pergunta: “Mas… O que você tem?”"]) {
        [self playSound:@"oqVcTem"];
        textoFalado = @"";
    }
//    if ([textoFalado isEqualToString:@"Certo dia, Eu estava no shopping, com meu amigo, e decidi comprar um livro..."]) {
//        [self playSound:@"foforin"];
//        textoFalado = @"";
//    }
//    if ([textoFalado isEqualToString:@"Já existem soluções para uma pessoa com dificuldades para se expressar, como eu. Exemplo disso são os recursos utilizados aqui: este aplicativo, Voz, que foi desenvolvido por estudantes do Instituto Federal do Ceará. IFCE. A dança de cadeiras de rodas, entre outras coisas. É possível, para mim, sonhar com um futuro melhor. Mesmo com as sequelas da Paralisia Cerebral, sou feliz."]){
//        [self playSound:@"musicaDanca"];
//        textoFalado = @"";
//        
//    }
}

#pragma mark - functions que completam a tabela
-(void)completActions{
    action1 = [[Action alloc] init];
    
    action1.name = NSLocalizedString(@"Apresentação", nil);
    action1.sound = NSLocalizedString(@"", nil);
    action1.img = @"1";
    action1.background = [UIColor colorWithRed:72/255.0 green:207/255.0 blue:173/255.0 alpha:1];
    
    Action *um = [[Action alloc]init];
    um.name = NSLocalizedString(@"Olá pessoal", nil);
    um.sound = NSLocalizedString(@"Olá pessoal! Obrigada pela oportunidade de falar sobre inclusão, nesta Universidade que tanto amo. Meu nome é Danielle Cardoso, e todos me chamam de Dâni... Sou universitária, estou começando o sétimo semestre do curso de Letras da Universidade Estadual do Ceará (uéce). Estagio no Laboratório de Inclusão, onde sou facilitadora da oficina - Aprendendo a Viver com Acessibilidade. Lá, atendemos pessoas com deficiências. Física, Cognitiva, Visual e, Auditiva. Tenho um projeto chamado Redação Para a Vida, que tem como objetivo dar aulas online de redação para pessoas carentes. E tive Paralisia Cerebral. PC", nil);
    um.img = @"1";
    um.background = [UIColor colorWithRed:72/255.0 green:207/255.0 blue:173/255.0 alpha:1];
    
    
    Action *dois = [[Action alloc]init];
    dois.name = NSLocalizedString(@"estar se perguntando", nil);
    dois.sound = NSLocalizedString(@"Aí, vocês devem estar se perguntando. (“Como consigo superar isso?”). Bem, este é um recorte da minha primeira palestra que mostra, justamente, os desafios e a superação de uma pessoa com PC. Meu objetivo é; desmitificar algumas coisas criadas pelo senso comum e, pela falta de conhecimento da sociedade. Espero que gostem!", nil);
    dois.img = @"2";
    dois.background = [UIColor colorWithRed:59/255.0 green:173/255.0 blue:140/255.0 alpha:1];

    Action *tres = [[Action alloc]init];
    tres.name = NSLocalizedString(@"quando inicio uma conversa", nil);
    tres.sound = NSLocalizedString(@"Quando inicio uma conversa com uma pessoa desconhecida. Geralmente. Vem a famosa pergunta: “Mas… O que você tem?”", nil);
    tres.img = @"3";
    tres.background = [UIColor colorWithRed:72/255.0 green:207/255.0 blue:173/255.0 alpha:1];

    Action *quatro = [[Action alloc]init];
    quatro.name = NSLocalizedString(@"Perguntinha...", nil);
    quatro.sound = NSLocalizedString(@"Perguntinha para me irritar, eu tenho tanta coisa, como duas pernas, dois braços, uma cabeça...! Minha primeira reação é respirar fundo; continuar sendo simpática com a pessoa; e responder à pergunta dela: “Tenho PC!” (morro de preguiça de falar ou escrever Paralisia Cerebral).", nil);
    quatro.img = @"4";
    quatro.background = [UIColor colorWithRed:72/255.0 green:207/255.0 blue:173/255.0 alpha:1];
    
    Action *cinco = [[Action alloc]init];
    cinco.name = NSLocalizedString(@"O que é isso?", nil);
    cinco.sound = NSLocalizedString(@"“O que é isso?” É o que vem imediatamente depois. Minha maior felicidade é quando essa pergunta não aparece, mas isto é raro! E lá vou eu explicar o que é PC.", nil);
    cinco.img = @"5";
    cinco.background = [UIColor colorWithRed:59/255.0 green:173/255.0 blue:140/255.0 alpha:1];
    
    
    [action1 addChildren:um];
    [action1 addChildren:dois];
    [action1 addChildren:tres];
    [action1 addChildren:quatro];
    [action1 addChildren:cinco];
}

-(void)completActions2{
    action2 = [[Action alloc] init];
    
    action2.name = NSLocalizedString(@"Continuação", nil);
    action2.sound = NSLocalizedString(@"", nil);
    action2.img = @"2";
    action2.background = [UIColor colorWithRed:79/255.0 green:64/255.0 blue:119/255.0 alpha:1];
    
    Action *question1 = [[Action alloc]init];
    question1.name = NSLocalizedString(@"Por esse motivo", nil);
    question1.sound = NSLocalizedString(@"Por esse motivo, o primeiro passo que me veio à cabeça diante da ideia de uma palestra sobre Paralisia Cerebral foi conceituar esse “problema”. Claro que eu podia usar minhas palavras, porém, para ter maior embasamento teórico , pesquisei e encontrei a seguinte definição que Paula Nadal escreveu para o site Nova Escola:", nil);
    question1.img = @"1";
    question1.background = [UIColor colorWithRed:79/255.0 green:64/255.0 blue:119/255.0 alpha:1];
    
    Action *question2 = [[Action alloc]init];
    question2.name = NSLocalizedString(@"citação Paula", nil);
    question2.sound = NSLocalizedString(@"É uma lesão cerebral que acontece, em geral, quando falta oxigênio no cérebro do bebê durante a gestação, no parto ou até dois anos após o nascimento – neste caso, pode ser provocada por traumatismos, envenenamentos ou doenças graves, como sarampo ou meningite", nil);
    question2.img = @"2";
    question2.background = [UIColor colorWithRed:99/255.0 green:78/255.0 blue:146/255.0 alpha:1];
    
    Action *question3 = [[Action alloc]init];
    question3.name = NSLocalizedString(@"minha lesão", nil);
    question3.sound = NSLocalizedString(@"Minha lesão aconteceu no parto – assim dizem os médicos. Paula Nadal continua dizendo que, dependendo do local do cérebro onde ocorre a lesão e do número de células atingidas, a paralisia danifica o funcionamento de diferentes partes do corpo. A principal característica é a espasticidade (palavra difícil), que é um distúrbio nos movimentos da pessoa que causa tensão muscular e inclui dificuldades de força e de equilíbrio.", nil);
    question3.img = @"3";
    question3.background = [UIColor colorWithRed:79/255.0 green:64/255.0 blue:119/255.0 alpha:1];
    
    Action *question4 =  [[Action alloc]init];
    question4.name = NSLocalizedString(@"em outras palavras", nil);
    question4.sound = NSLocalizedString(@"Em outras palavras, a lesão provoca alterações na contração muscular e o comprometimento da coordenação motora. Em alguns casos, há também problemas na fala, na visão e na audição. E é por isso que vocês estão lendo este texto: além de ter comprometido minha coordenação motora, a paralisia também trouxe dificuldades para a minha dicção.", nil);
    question4.img = @"4";
    question4.background = [UIColor colorWithRed:79/255.0 green:64/255.0 blue:119/255.0 alpha:1];
    
    Action *question5 = [[Action alloc]init];
    question5.name = NSLocalizedString(@"sei que toda essa", nil);
    question5.sound = NSLocalizedString(@"Sei que toda essa definição deixou o texto um pouco chato, mas era preciso entender a tal da PC. Quero acrescentar que Paralisia Cerebral não é doença (pelo amor de tudo que é mais sagrado, entendam isto!). PC não pega, não melhora, não piora e, o mais importante, não tem cura, exatamente por não ser uma doença! Entenderam, não é?", nil);
    question5.img = @"5";
    question5.background = [UIColor colorWithRed:99/255.0 green:78/255.0 blue:146/255.0 alpha:1];
    
    
    [action2 addChildren:question1];
    [action2 addChildren:question2];
    [action2 addChildren:question3];
    [action2 addChildren:question4];
    [action2 addChildren:question5];
}

-(void)completActions3{
    action3 = [[Action alloc] init];
    
    action3.name = NSLocalizedString(@"Adversidades", nil);
    action3.sound = NSLocalizedString(@"", nil);
    action3.img = @"3";
    action3.background = [UIColor colorWithRed:160/255.0 green:212/255.0 blue:104/255.0 alpha:1];
    
    Action *question1 = [[Action alloc]init];
    question1.name = NSLocalizedString(@"estou insistindo", nil);
    question1.sound = NSLocalizedString(@"Estou insistindo nisso porque uma das coisas que eu detesto (e acredito que todos que têm alguma deficiência) é ser chamada de doente ou ouvir certas coisas, como: “Você vai ficar boa!”, “Você vai andar!”. Pois é, isso ainda existe e vocês não imaginam o quanto. A verdade é que não vou ficar boa (poderia dizer que já sou, mas, enfim…) nem vou andar. O que eu faço é viver como posso e assim aprendo a superar a PC.", nil);
    question1.img = @"1";
    question1.background = [UIColor colorWithRed:160/255.0 green:212/255.0 blue:104/255.0 alpha:1];
    
    Action *question2 = [[Action alloc]init];
    question2.name = NSLocalizedString(@"outro ponto importante", nil);
    question2.sound = NSLocalizedString(@"Outro ponto importante para se abordar é que: apesar da Paralisia Cerebral também poder atingir a parte intelectual – cerca de 75% das crianças com PC tem essa parte comprometida, ainda segundo Paula Nadal – você não precisa ver uma pessoa como eu, e tratar como se ela tivesse três anos de idade. E mesmo se a pessoa tiver complicações na cognição, você também não precisa tratá-la como uma doente mental. Isso é muito constrangedor! Deixem-me contar uma situação que aconteceu comigo, aí, vocês se põem no meu lugar, está certo?", nil);
    question2.img = @"2";
    question2.background = [UIColor colorWithRed:140/255.0 green:178/255.0 blue:89/255.0 alpha:1];
    
    Action *question3 =  [[Action alloc]init];

    question3.name = NSLocalizedString(@"certo dia...", nil);
    question3.sound = NSLocalizedString(@"Certo dia, eu estava no shopping com minha amiga e decidi comprar um livro. Estava tudo bem até eu ver a seção infantil do outro lado da loja. Eu tenho um irmão de 4 anos, por isso a seção me chamou atenção, queria comprar algo para ele. Só que a vendedora não entendeu isso. Essa mulher começou a me tratar de um jeito que nem meu irmão ia gostar. E por mais que tentássemos explicar que eu não era uma débil mental, mas ela exagerava. Paguei meu livro e a lembrancinha do meu irmão e saí da loja. Nunca mais voltei lá!", nil);
    question3.img = @"3";
    question3.background = [UIColor colorWithRed:160/255.0 green:212/255.0 blue:104/255.0 alpha:1];
    
    Action *question4 = [[Action alloc]init];
    question4.name = NSLocalizedString(@"vocês podem até ", nil);
    question4.sound = NSLocalizedString(@"Vocês podem até pensar que é fácil para mim passar por tudo isso. Bem, não é! Sou tetraplégica e, consequentemente, enfrento muitas adversidades.", nil);
    question4.img = @"4";
    question4.background = [UIColor colorWithRed:160/255.0 green:212/255.0 blue:104/255.0 alpha:1];
    
    Action *question5 = [[Action alloc]init];
    question5.name = NSLocalizedString(@"Primeira", nil);
    question5.sound = NSLocalizedString(@"Primeira: na maioria do tempo, preciso dos cuidados de uma pessoa, especificamente, da minha mãe. Então, desde do meu levantar ao meu deitar, preciso dela. É banho, locomoção, vestir e tirar roupa, higiene pessoal… Enfim, tudo. Aí, vocês podem imaginar o tão complicado é isso. Temos que ter muita empatia para entender uma a outra e nem sempre isto é possível. Somos pessoas diferentes, claro. Ela tem suas limitações também e nem sempre quando estou bem, ela também está (e vice-versa).", nil);
    question5.img = @"5";
    question5.background = [UIColor colorWithRed:140/255.0 green:178/255.0 blue:89/255.0 alpha:1];
    
    
    [action3 addChildren:question1];
    [action3 addChildren:question2];
    [action3 addChildren:question3];
    [action3 addChildren:question4];
    [action3 addChildren:question5];

}

-(void)completActions4{
    action4 = [[Action alloc] init];
    action4.name = NSLocalizedString(@"Superações", nil);
    action4.sound = NSLocalizedString(@"", nil);
    action4.img = @"4";
    action4.background = [UIColor colorWithRed:185/255.0 green:37/255.0 blue:174/255.0 alpha:1];
    
    Action *um = [[Action alloc] init];
    um.name = NSLocalizedString(@"segundo", nil);
    um.sound = NSLocalizedString(@"Segunda: como uma amiga disse, o mundo é feito para os “normais”. Eu não posso derrubar o apartamento onde moro para adaptá-lo. Estudo num local que não existe acessibilidade (quem conhece o Centro de Humanidades da UÉCE, sabe do que estou falando)… Estou há três anos lá e o prédio nunca foi reformado em relação à acessibilidade. Fico triste ao pensar que sairei do CH sem que lá tenha um elevador. Sem falar na invisibilidade que enfrento, não só no CH, mas em toda a sociedade. Não é difícil encontrar professores que fingem não me ver… Com tudo isso, vamos nos virando, adaptando como podemos. Não é fácil, mas vamos vencendo o gigante nosso de cada dia.", nil);
    um.img = @"1";
    um.background = [UIColor colorWithRed:185/255.0 green:37/255.0 blue:174/255.0 alpha:1];
    
    
    Action *dois = [[Action alloc] init];
    dois.name = NSLocalizedString(@"Isso traz desgaste", nil);
    dois.sound = NSLocalizedString(@"Isso traz desgastes psicológicos, óbvio. Há o pensamento inevitável de que sou um peso para minha mãe ou de que como vai ser no futuro e tal. Estes pensamentos, às vezes, causam lágrimas e desespero. Não tenho motivos para esconder tais coisas. No entanto, dia a dia, nós nos superamos e aprendemos um pouquinho mais a lidar com tudo isso. Há diversos outros problemas, que prefiro não tratar como tal, se não, enlouqueço. Melhor pensar que a vida é assim mesmo e que sem estes obstáculos não teria graça viver. E vejo que o problema não sou eu e, sim, o olhar do outro, que não aceita o diferente, não está aberto a mudanças, dentre outras coisas.", nil);
    dois.img = @"2";
    dois.background = [UIColor colorWithRed:214/255.0 green:93/255.0 blue:217/255.0 alpha:1];
    
    Action *tres = [[Action alloc] init];
    tres.name = NSLocalizedString(@"disse muitas coisas", nil);
    tres.sound = NSLocalizedString(@"Bem, disse muitas coisas. Mas ainda não contei minha história e, acho importante vocês entenderem como tudo aconteceu na minha vida. E que nela não existe só coisas ruins, óbvio. Como já disse, tive Paralisia Cerebral ao nascer e isto comprometeu meus movimentos e minha voz, mas não minha força de vontade!", nil);
    tres.img = @"3";
    tres.background = [UIColor colorWithRed:185/255.0 green:37/255.0 blue:174/255.0 alpha:1];
    
    Action *quatro = [[Action alloc] init];
    quatro.name = NSLocalizedString(@"desde muito pequena", nil);
    quatro.sound = NSLocalizedString(@"Desde muito pequena, fui incentivada a aprender a conviver e superar a minha deficiência. E aqui, ressalto a importância da minha família maravilhosa nesse processo. Se minha mãe não tivesse entendido desde o começo que minhas dificuldades são apenas motoras e sempre tivesse me tratado como uma pessoa com deficiência mental, eu não estaria aqui, hoje, dando esta palestra. Nossa luta começou quando eu tinha 10 meses, foi quando comecei a fazer fisioterapia. Minha infância foi normal. Saía, brincava, fazia birra e estudava. Sempre estudei. Com 7 anos, alfabetizei-me na mesma instituição em que fazia fisioterapia, mas também tive muito incentivo da minha mãe em casa. Depois disso, fui para uma escola regular. Daí, não parei mais! Além de estudar, nessa época, passei a fazer dança em cadeira de rodas. Comecei no hospital Sarah e depois fui para a Associação Elos da Vida.", nil);
    quatro.img = @"4";
    quatro.background = [UIColor colorWithRed:185/255.0 green:37/255.0 blue:174/255.0 alpha:1];
    
    Action *cinco = [[Action alloc] init];
    cinco.name = NSLocalizedString(@"com 16 anos", nil);
    cinco.sound = NSLocalizedString(@"Com 16 anos, fui para uma escola pública fazer meu ensino médio. Foi a melhor escola que estudei. Não que nas outras eu não tivesse professores e amigos muito bons, mas, quando se é adolescente, tudo se intensifica. Não fui imune a isso. Tive professores inesquecíveis, tanto no lado bom como no ruim (sim, sofri preconceitos de professores); encontrei amigos muito especiais, tive um amor platônico… Tudo que uma adolescente tem direito, eu tive.", nil);
    cinco.img = @"5";
    cinco.background = [UIColor colorWithRed:214/255.0 green:93/255.0 blue:217/255.0 alpha:1];

    
    [action4 addChildren:um];
    [action4 addChildren:dois];
    [action4 addChildren:tres];
    [action4 addChildren:quatro];
    [action4 addChildren:cinco];
    
    
}

-(void)completActions5{
    action5 = [[Action alloc] init];
    action5.name = NSLocalizedString(@"Encerramento", nil);
    action5.sound = NSLocalizedString(@"", nil);
    action5.img = @"5";
    action5.background = [UIColor colorWithRed:79/255.0 green:193/255.0 blue:233/255.0 alpha:1];
    
    Action *um = [[Action alloc] init];
    um.name = NSLocalizedString(@"Uma pessoa que basicamente", nil);
    um.sound = NSLocalizedString(@"Uma pessoa que basicamente mudou minha vida foi o diretor dessa escola, na época. Ele me incentivou muito a prestar vestibular e passei para Letras na uéci, o mesmo curso que ele fez. Foi uma guerra para eu poder ocupar essa vaga. Eu não tinha condições de ir assistir às aulas todos os dias. Não conseguia chegar na hora. Pois não dava para subir nos ônibus, pela lotação. Nem todos eram adaptados.", nil);
    um.img = @"1";
    um.background = [UIColor colorWithRed:79/255.0 green:170/255.0 blue:200/255.0 alpha:1];
    
    
    Action *dois = [[Action alloc] init];
    dois.name = NSLocalizedString(@"Consegui um transporte", nil);
    dois.sound = NSLocalizedString(@"Consegui um transporte da própria uéci e hoje estou no sexto semestre desse curso lindo. Tenho amigos maravilhosos, que me ajudam em tudo que podem e, às vezes, até esquecem que sou deficiente, pois me tratam normalmente, não enxergam somente uma garota numa cadeira de rodas, enxergam alguém como eles, capaz de quase tudo, inteligente ao ponto de me indicarem para oportunidades incríveis dentro da Universidade.", nil);
    dois.img = @"2";
    dois.background = [UIColor colorWithRed:79/255.0 green:193/255.0 blue:233/255.0 alpha:1];
    
    Action *tres = [[Action alloc] init];
    tres.name = NSLocalizedString(@"Recentemente, conheci", nil);
    tres.sound = NSLocalizedString(@"Recentemente, conheci o Laboratório de Inclusão, que como o nome sugere, trabalha com a inclusão de pessoas com deficiência e vulnerabilidade social. Passei na seleção de estagiários e estou amando trabalhar neste local. Estou descobrindo que ter uma deficiência como a minha é só um detalhe e que talvez a sociedade esteja mudando. Por quê não?", nil);
    tres.img = @"3";
    tres.background = [UIColor colorWithRed:79/255.0 green:170/255.0 blue:200/255.0 alpha:1];
    
    Action *quatro = [[Action alloc] init];
    quatro.name = NSLocalizedString(@"já que existem soluções", nil);
    quatro.sound = NSLocalizedString(@"Já existem soluções para uma pessoa com dificuldades para se expressar, como eu. Exemplo disso são os recursos utilizados aqui: este aplicativo, Voz, que foi desenvolvido por estudantes do Instituto Federal do Ceará. IFCE. A dança de cadeiras de rodas, entre outras coisas. É possível, para mim, sonhar com um futuro melhor. Mesmo com as sequelas da Paralisia Cerebral, sou feliz.", nil);
    quatro.img = @"4";
    quatro.background = [UIColor colorWithRed:79/255.0 green:170/255.0 blue:200/255.0 alpha:1];
    
    Action *fim = [[Action alloc] init];
    fim.name = NSLocalizedString(@"para terminar", nil);
    fim.sound = NSLocalizedString(@"Para terminar, gostaria de dizer para vocês nunca deixarem de sonhar. Nunca cale suas ideias, elas têm valor. Não desista nunca, pois, como diz meu lema de vida, quem acredita sempre alcança!", nil);
    fim.img = @"5";
    fim.background = [UIColor colorWithRed:79/255.0 green:193/255.0 blue:233/255.0 alpha:1];
    
    [action5 addChildren:um];
    [action5 addChildren:dois];
    [action5 addChildren:tres];
    [action5 addChildren:quatro];
    [action5 addChildren:fim];

    
}
@end
