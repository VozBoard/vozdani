//
//  ViewController.h
//  KeyBoardAdapt
//
//  Created by Bruno Roberto on 13/12/14.
//  Copyright (c) 2014 bruno. All rights reserved.
// Elton Nobre

#import <UIKit/UIKit.h>

@import AVFoundation;
@interface ViewController : UIViewController<NSStreamDelegate,AVSpeechSynthesizerDelegate,UITextViewDelegate>


@property (weak, nonatomic) IBOutlet UITextView *outletText;


@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *listKeyBoard;

@property (weak, nonatomic) IBOutlet UIButton *outletKeyConfig;

- (IBAction)keyConfig:(UIButton *)sender;
- (IBAction)actionsKey:(UIButton *)sender;
- (IBAction)switchKey:(UIButton *)sender;
- (IBAction)keySpace:(UIButton *)sender;
- (IBAction)keyDelete:(UIButton *)sender;
- (IBAction)keyPause:(UIButton *)sender;

- (IBAction)playSpeak:(UIButton *)sender;

- (IBAction)keyCursorEsquerda:(UIButton *)sender;
- (IBAction)keyCursorDireita:(UIButton *)sender;

- (IBAction)keyCapsLoker:(UIButton *)sender;

- (IBAction)btnVoltar:(id)sender;


@end

